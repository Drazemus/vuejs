import {sortTable} from '../../data/data.js'
  
// Filtre le tableau sur l'indice demandé
export function filterArray (array, indice) {
    var tab = array.map((element) => element[indice]).flat()
    return tab.filter((element, index, array) => index === array.indexOf(element))
}

// Retourne le nombre de livres
export function countBooks () {
    var books = filterArray(sortTable, 0)
    return books.length
}
// Retourne le nombre d'écoles
export function countSchools () {
    var schools = filterArray(sortTable, 2)
    return schools.length
}

// Retourne le nombre de sorts
export function countSorts () {
    return sortTable.length
}