import Vue from 'vue'
import Router from 'vue-router'

import Configuration from '../components/Configuration.vue'
import Recherche from '../components/Recherche.vue'
import Statistique from '../components/Statistique.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'config',
            component: Configuration
        },
        {
            path: '/recherche',
            name: 'search',
            component: Recherche
        },
        {
            path: '/statistique',
            name: 'stats',
            component: Statistique
        }
    ]
})